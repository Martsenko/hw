import gulp from 'gulp';
// const gulp = require('gulp')

import concat from 'gulp-concat';
// const concat = require('gulp-concat');

import autoprefixer from 'gulp-autoprefixer';
// const autoprefixer = require('gulp-autoprefixer');

import cleanCSS from 'gulp-clean-css';
// const cleanCSS = require('gulp-clean-css');

import uglify from 'gulp-uglify';
// const uglify = require('gulp-uglify');
import imagemin from 'gulp-imagemin';

import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass( dartSass );
// const sass = require("gulp-sass")(require("sass"));


import { deleteAsync } from 'del';
import browserSync from 'browser-sync';
// const del = require('del');

import fileinclude from 'gulp-file-include';
// const fileinclude = require('gulp-file-include');


const cssFiles = [
    './src/scss/general_scss.scss',
];
const jsFiles = [
    './src/js/general_js.js',
];
const htmlFiles = [
    'general.html',
]

function html() {
    return gulp.src(htmlFiles)
    .pipe(concat('index.html'))
    .pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'))
    .pipe(browserSync.stream())
}

function styles() {
    return gulp.src(cssFiles)
        .pipe(sass().on("error", sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist:['> 0.1%'],
			cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function scripts() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify({
            toplevel:true,
        }))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
};

function compress_image() {
    return gulp.src('./src/img/**/*.*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img/'))
    .pipe(browserSync.stream());
};

function watch() {
    browserSync.init({
        server: {
            baseDir:"./dist/"
        },
        browser: "google chrome",
        open: true,
    })
    gulp.watch('./src/scss/**/*.scss', styles);
    gulp.watch('./src/js/**/*.js', scripts);
    gulp.watch('./src/html/**/*.html', html);
    gulp.watch('*.html', html);
    gulp.watch('./src/img/**/*.*')
    gulp.watch("./src/*.html").on("change", browserSync.reload);
};

function clean() {
    return deleteAsync(['dist/**'])
}


gulp.task('watch', watch);

gulp.task('build', gulp.series(clean,
    gulp.parallel(html, scripts, compress_image, styles)));

gulp.task('dev', gulp.parallel(watch));


