'use strict'
const headerBtn = document.querySelector('.header-button');
const headerNav = document.querySelector('.header-nav');
const headerBtnIcon = document.querySelector('.header-button__icon');
headerBtn.addEventListener('click', (headerBtn) => {
    headerNav.classList.toggle('header-nav--active');
    headerBtnIcon.classList.toggle('header-button__icon--active');
})

window.addEventListener('resize', () => { 
    headerNav.classList.remove('header-nav--active');
    headerBtnIcon.classList.remove('header-button__icon--active');
})



