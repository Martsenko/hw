import React, { Component } from "react";
import "./modals/ModalOne.scss"

class Modal extends Component {
    render() {
      const{header, text, closeButton, className, actions} = this.props
      return (
        <div
          className={className}
          onClick={() => {
            closeButton();
          }}
        >
          <div
            className="modal-content"
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <div className="modal-content__header">
              <p>{header}</p>
              <button
                className="modal-content__close"
                onClick={() => {
                  closeButton();
                }}
              >
                x
              </button>
            </div>

            <p className="modal-content__text">{text}</p>
            <div className="modalButtons">
              {actions}
            </div>
          </div>
        </div>
      );
  }
}

export default Modal;
