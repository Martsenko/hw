import React, { Component } from 'react'
import "./Main.scss"
import CardItem from './products/CardItem';


class Main extends Component {
  render() {
    return (
      <div className="main">
        <div className="list">
          <CardItem/>
        </div>
      </div>
    );
  }
}

export default Main;

