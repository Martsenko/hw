import React, { Component } from 'react'
import "./CardItem.scss"

class CardItem extends Component {
  render() {
    return (
      <>
        <div className="card">
          <img className='card__star' src='#'></img>
          <span className="card__name">tovar</span>
          <img src="./google_logo.svg"></img>
          <span className="articul">articul : 1244</span>
          <span className="color">color: red</span>
          <span className="card__price">25$</span>
        </div>
      </>
    );
  }
}

export default CardItem;