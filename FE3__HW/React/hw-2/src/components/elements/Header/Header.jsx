// import PropTypes from 'prop-types'
import React, { Component } from "react";
import "./Header.scss"

class Header extends Component {
  //   static propTypes = {second: third}

  render() {
    return (
      <div className="header">
        <div className="header__info-bar">
          <div className="header__social">інстаграм твітер і так далі</div>
          <div className="header__right-bar">
            <div className="header__login">
              <span>Login </span>/<span> Register</span>
            </div>
            <div className="basket"> ikon Cart(2)</div>
          </div>
        </div>
        <div className="header__nav-bar">
          logo
          <nav className="header__nav">
            <a href="#">Home</a>
            <a href="#">Buy</a>
            <a href="#">Busket</a>
            <a href="#">About Us</a>
            <a href="#">Contacts</a>
          </nav>
        </div>
      </div>
    );
  }
}

export default Header;
