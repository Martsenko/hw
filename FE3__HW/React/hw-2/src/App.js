import React from "react";
import "./App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";
import Header from "./components/elements/Header/Header.jsx";
import Main from "./components/elements/Main/Main.jsx";
import Footer from "./components/elements/Footer/Footer";

const products = [
  {
    name: "амфітамін",
    price: 15, //додати на сайті валюту
    image: "./google_logo.svg",
    articul: 12345,
    color: "yellow",
  },
  {
    name: "кокаїн",
    price: 20, //додати на сайті валюту
    image: "./google_logo.svg",
    articul: 12345,
    color: "white",
  },
  {
    name: "маріхуана",
    price: 25, //додати на сайті валюту
    image: "./google_logo.svg",
    articul: 12345,
    color: "green",
  },
];

class App extends React.Component {
  state = {
    header: (null),
    text: (null),
    closeButton: false,
    class: (null),
    actions: (null),
  };

  closeButton = () => {
    this.setState({ closeButton: false });
  };

  onClickEnter = () => {
    this.setState({
      closeButton: true,
      header: "Do you want to delete this file?",
      text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
      className: "modal-one",
      actions: (
        <>
          <Button text={"Ok"} onClick={this.closeButton} />
          <Button text={"Cancel"} onClick={this.closeButton} />
        </>
      ),
    });
  };

  onClickRegister = () => {
    this.setState({
      closeButton: true,
      header: "Lorem ipsum dolor?",
      text: " Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum quis explicabo? Id similique quae quas.",
      className: "modal-two",
    });
  };

  render() {
    const backgroundEnter = "rgb(168, 162, 162, 0.5)";
    const backgroundRegister = "rgb(218, 213, 164, 0.5)";
    const textEnter = "Add to cart";
    const textRegister = "Register";

    return (
      <div className="App">
        <Header />
        <Main />
        <Footer />
        <Button className="btn"
          backgroundColor={backgroundEnter}
          text={textEnter}
          onClick={this.onClickEnter}
        />
        {/*
        <Button
          backgroundColor={backgroundRegister}
          text={textRegister}
          onClick={this.onClickRegister}
        /> */}

        {this.state.closeButton && (
          <Modal
            actions={this.state.actions}
            header={this.state.header}
            text={this.state.text}
            className={this.state.className}
            closeButton={this.closeButton}
          />
        )}
      </div>
    );
  }
}


export default App;
