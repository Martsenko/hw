import React from "react";
import "./App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";

class App extends React.Component {
  state = {
    header: (null),
    text: (null),
    closeButton: false,
    class: (null),
    actions: (null),
  };

  closeButton = () => {
    this.setState({ closeButton: false });
  };

  onClickEnter = () => {
    this.setState({
      closeButton: true,
      header: "Do you want to delete this file?",
      text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
      className: "modal-one",
      actions: (
        <>
          <Button text={"Ok"} onClick={this.closeButton} />
          <Button text={"Cancel"} onClick={this.closeButton} />
        </>
      ),
    });
  };

  onClickRegister = () => {
    this.setState({
      closeButton: true,
      header: "Lorem ipsum dolor?",
      text: " Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas cum quis explicabo? Id similique quae quas.",
      className: "modal-two",
    });
  };

  render() {
    const backgroundEnter = "rgb(168, 162, 162, 0.5)";
    const backgroundRegister = "rgb(218, 213, 164, 0.5)";
    const textEnter = "Enter";
    const textRegister = "Register";

    return (
      <div className="App">
        <Button
          backgroundColor={backgroundEnter}
          text={textEnter}
          onClick={this.onClickEnter}
        />
        <Button
          backgroundColor={backgroundRegister}
          text={textRegister}
          onClick={this.onClickRegister}
        />

        {this.state.closeButton && (
          <Modal
            actions={this.state.actions}
            header={this.state.header}
            text={this.state.text}
            className={this.state.className}
            closeButton={this.closeButton}
          />
        )}
      </div>
    );
  }
}


export default App;
