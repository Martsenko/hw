"use strict";
const url = "https://ajax.test-danit.com/api/json";
const root = document.querySelector("#root");

class Remover {
  remove_post(event, url, post_id) {
    fetch(`${url}` + `/posts/${post_id}`, {
      method: "DELETE",
    })
      .then((resp) => {
        if (resp.status == 200) {
          console.log(`${post_id} - REMOVED!` );
          event.target.parentElement.remove();
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }
}

class User {
  constructor(url, root, entity) {
    this.url = url;
    this.root = root;
    this.entity = entity;
  }
  request(link) {
    return fetch(link)
      .then((resp) => resp.json())
      .then((data) => data);
  }

  renderTweetWrap() {
    this.template = document.querySelector("#template").content;
    this.name = this.template.querySelector(".name").cloneNode();
    this.email = this.template.querySelector(".email").cloneNode();
  }
  render() {
    this.request(`${this.url}/users`).then((users) => {
      this.renderTweetWrap();
      this.request(`${this.url}/posts`).then((posts) => {
        posts.forEach((post) => {
          this.tweetWrap = this.template
            .querySelector(".tweet-wrap")
            .cloneNode(true);
          root.append(this.tweetWrap);

          this.tweetHeaderInfo =
            this.template.querySelector(".tweet-header-info");

          this.tweetHeaderInfo.innerHTML = `
            <span class="tweet-day">02.04.2022</span>
            <p class="tweet-desc-title"> ${post.title}</p>
            <p class="tweet-desc"> ${post.body}</p>
          `;

          users.forEach((user) => {
            if (user.id === post.userId) {
              this.tweetHeaderInfo.insertAdjacentHTML( "afterbegin",
                `<span class="name">${user.name}</span>
                <br>
                <span class="email">${user.email}</span> 
                <br>
                `
              );
            }
          });

          const btn = document.createElement("button");
          this.tweetWrap.append(btn);
          btn.classList.add("button");
          btn.type = "button";
          btn.textContent = "x";

          btn.addEventListener("click", (event) => {
            const remover = new Remover();

            remover.remove_post(event, url, post.id);
          });
        });
      });
    });
  }
}

new User(url, root).render();
