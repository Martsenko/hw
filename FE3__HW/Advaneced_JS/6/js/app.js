"use strict";

const url = "https://api.ipify.org";
const endpoint =
  "http://ip-api.com/json/?fields=continent,country,regionName,city,district";

const btn = document.querySelector(".button");
const container = document.querySelector("#container");


class User {
  constructor(endpoint, container) {
    this.endpoint = endpoint;
    this.container = container;
  }
  render() {
    this.template = document.querySelector(".template").content;
    this.infoDiv = this.template.querySelector(".info").cloneNode();
    console.log(container);
    container.append(this.infoDiv);
  }
  getInfo() {
    async function request() {
      try {
        const resp = await fetch(endpoint);
        const data = await resp.json();
        return data;
      } catch (e) {
        console.error(r);
      }
    }
    request().then((data) => {
      this.render();
      this.infoDiv.innerHTML = `
      <p class="continent">continent: ${data.continent}</p>
      <p class="country">country: ${data.country}</p>
      <p class="region">region: ${data.regionName}</p>
      <p class="city">city: ${data.city}</p>
      <p class="district">district: ${data.district}</p>
      `
    })
  }
  
}


class Info {
  constructor(url, btn) {
    this.url = url;
    this.btn = btn;
  }
  request() {
    this.btn.addEventListener("click", (event) => {
      async function request() {
        try {
          const resp = await fetch(url + "/?format=json");
          const data = await resp.json();
          if (resp.status === 200) {
            new User().getInfo();
          }
          return data;
        } catch (e) {
          console.error(e);
        }

      }
      request()
    });
  }
}
new Info(url, btn).request();





