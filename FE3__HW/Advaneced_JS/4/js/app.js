const url = "https://ajax.test-danit.com/api/swapi/";
const root = document.querySelector("#root");


class Films {
  constructor(url, root, entity) {
    this.url = url;
    this.root = root;
    this.entity = entity;
  }

  request(url = this.url, entity = "", id = "") {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open("GET", `${url}${entity}${id}`);
      xhr.send();

      xhr.onload = () => resolve(xhr.response);
      xhr.onerror = () => reject(e);
    });
  }

  render() {
    const list = document.createElement("ul");
    this.request(`${this.url}films`).then((response) => {
      const data = JSON.parse(response);
      const items = data.map(
        ({ episodeId, name, characters, openingCrawl }) => {
          const listItem = document.createElement("li");
          const filmName = document.createElement("span");
          filmName.innerText = "episode" + " " + episodeId + " - " + name;
          const description = document.createElement("p");
          description.innerText = openingCrawl;
          listItem.append(filmName);
          listItem.append(description);

          const charactersInfo = characters.map((character) =>
            this.request(character)
          );

          Promise.allSettled(charactersInfo).then((el) => {
            el.forEach(({ value }) => {
              const name = JSON.parse(value).name;
              const filmCharacters = document.createElement("p");
              filmCharacters.innerText = name;
              listItem.append(filmCharacters);
            });
          });
          return listItem;
        }
      );

      list.append(...items);
      root.append(list);
    });
  }
}

const entity = "films";
const films = new Films(url, entity, root);
films.render();

