"use strict"
class Libary {
    constructor(author, name, price) {
        this.author = author;
        this.name = name;
        this.price = price;
    }
  render(root) {
    this.list = document.createElement("ul");
    }
}

class Shelf extends Libary {
  constructor(author, name, price, books) {
    super(author, name, price);
    this.books = books;
  }
  render(root) {
    super.render(root);
    root.append(this.list)
    const books = [
      {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
      },
      {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
      },
      {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
      },
      {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
      },
      {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
      },
      {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
      }
    ];

    books.forEach(book => {
      this.item = document.createElement("li");
      try {
          if (book.author == undefined) { throw 'Author is undefined';}
          if (book.name == undefined) { throw 'Name is undefined';}
          if (book.price == undefined) { throw 'Price is undefined';}
          this.list.append(this.item);
          this.title = document.createElement("p");
          this.item.append(this.title);
          
          this.title.append(book.author);
          this.title.append(book.name);
          this.title.append(book.price);

        }
        catch (e) {
          console.error(e);
        } 
      
    });
  }
}
this.root = document.querySelector('#root');
const shelfBooks = new Shelf();
shelfBooks.render(root);
