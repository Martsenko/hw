
class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;

    }

    salaryProgramer() { 
        this.salary = this.salary * 3
    }
    get getName() {
        return this.getName;
    }
    get getAge() {
        return this.age;
    }
    get getSalary() {
        return this.salary;
    }
    set changeName(newName) {
        this.name = newName;
    }
    set changeAge(newAge) {
        this.age = newAge;
    }
    set changeSalary(newSalary) {
        this.salary = newSalary;
    }

}

class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
        this.salaryProgramer()
    }

    
    get getSalary() {
        return this.salary;
    }
    set changeSalary(newSalary) {
        this.salary = newSalary;
    }
    
}

const employee = new Employee("Vlad", 19, 2000);

const programmer1 = new Programmer("Ivan", 26, 2000, "ua, hindi, en, ba");
const programmer2 = new Programmer("John", 30, 3000, "js, react, c++, java");


console.log(programmer1);
console.log(programmer2);
console.log(programmer1.getSalary);
console.log(programmer2.getSalary);


