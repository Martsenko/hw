'use strict'
const slides = document.querySelectorAll('.images-wrapper .image-to-show');
let carrenSlide = 0;
let go = setInterval(carusel, 3000);
let btnStart = document.getElementById('start');
let btnStop = document.getElementById('stop');
btnStart.addEventListener('click', start);
btnStop.addEventListener('click', pause);

function carusel() {
    slides[carrenSlide].className = "image-to-show";
    carrenSlide = (carrenSlide + 1) % slides.length;
    slides[carrenSlide].className = "show";
}
function pause() {
    clearInterval(go)
}
function start() {
    go = setInterval(carusel, 3000);
}