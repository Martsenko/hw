"use strict";
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraph = document.getElementsByTagName("p");
for (let i = 0; i < paragraph.length; i++){
    paragraph[i].style.backgroundColor = '#ff0000';
}

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const elem = document.getElementById('optionsList');
console.log(elem);

const parElem = document.getElementById('optionsList').parentElement;
console.log(parElem);

const childElemList = document.getElementById('optionsList').childNodes;
console.log(childElemList);

// Встановіть в якості контента елемента з id testParagraph наступний параграф -This is a paragraph
document.getElementById('testParagraph').textContent = `This is a paragraph`;

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const addClass1 = document.querySelector(".main-header");
let elel = addClass1.getElementsByTagName('li')
const addClass = document.querySelector(".main-header").childNodes;
console.log(addClass);
for (let i = 0; i < addClass.length; i++){
    addClass[i].className += " nav-item";
}

// Знайти всі елементи із класом options-list-title. Видалити цей клас у цих елементів.
const deleteClass = document.querySelectorAll(".options-list-title");
for (let i = 0; i < deleteClass.length; i++){
    deleteClass[i].classList.remove("options-list-title")
}
console.log(deleteClass);