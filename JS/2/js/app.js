'use strict';

let name;
do {
    name = prompt("Your name");
} while (!isNaN(name) || name === null || name === "");

let age;
do {
    age = prompt("Your age");
} while (isNaN(age) || age === null || age === "");

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
    alert(`Welcome, ${name}`);
    } else {
    alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name}`);
}
