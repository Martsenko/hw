'use strict'

const inputs = document.querySelectorAll('.inputPass');
const icon = document.querySelectorAll('i.icon-password');

icon.forEach(function (ele) {
    ele.addEventListener('click', function (e) {
        const targetInput = e.target.previousElementSibling.getAttribute('type');
        if (targetInput == 'password') {
            e.target.previousElementSibling.setAttribute('type', 'text');
            ele.classList.remove('fa-eye-slash');
            ele.classList.add('fa-eye');
        } else if (targetInput == 'text') {
            e.target.previousElementSibling.setAttribute('type', 'password');
            ele.classList.add('fa-eye-slash');
            ele.classList.remove('fa-eye');
        }
    });
});

const button = document.getElementById('btn');
button.addEventListener('click', function () {
    let firstPass = document.getElementById('firstPastInput');
    let secondPass = document.getElementById('secondPastInput');
    if (firstPass.value === secondPass.value) {
        alert(`You are welcome`);
    } else {
        let textAlert = document.createElement('p');
        textAlert.textContent = `Потрібно ввести однакові значення`;
        let secondLabel = document.getElementById('secondLabel');
        textAlert.style.margin = '0';
        secondPass.style.marginBottom = '0';
        secondLabel.appendChild(textAlert);
    }
});

