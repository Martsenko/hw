'use strict'
    + function () {
    document.querySelector('.tabs-content-text').classList.add('active');
    
    function selectPanel(e) {
        let target = e.target.dataset.target;
        document.querySelectorAll('.tabs-title, .tabs-content-text').forEach(el => {el.classList.remove('active')}); 
        e.target.classList.add('active');
        document.querySelector('.' + target).classList.add('active');
    }
    
document.querySelectorAll('.tabs-title').forEach(el => {el.addEventListener('click', selectPanel)}); 
}()