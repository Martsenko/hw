const btn = document.getElementById('btn')
const body = document.getElementById('body');
let cardEl = [...document.getElementsByClassName('card-details')];

let storagee = {
    state: 'dark',
    changeTheme: function() {
    if (body.classList.contains('body-theme')) {
        body.classList.remove('body-theme')
    } else {
        body.classList.add('body-theme')
    }
        for (let i = 0; i < cardEl.length; i++){
        if (cardEl[i].classList.contains('card-details-theme')) {
            cardEl[i].classList.remove('card-details-theme')
        } else {
            cardEl[i].classList.add('card-details-theme')
        }
        }
        if (btn.classList.contains('btn-color')) {
        btn.classList.remove('btn-color')
    } else {
        btn.classList.add('btn-color')
        }
        if (storagee.state === 'light') {
            storagee.state = 'dark'
        } else { 
            storagee.state = 'light'
        }
        localStorage.setItem('themeState', storagee.state);
    },
}
btn.addEventListener('click', storagee.changeTheme)

function checklocalStorage() { 
    let currrentThemeState = localStorage.getItem('themeState');
    if (currrentThemeState === "light") storagee.changeTheme()
}

checklocalStorage()