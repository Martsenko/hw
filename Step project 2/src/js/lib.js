'use strict'
const headerBtn = document.querySelector('.header-button');
headerBtn.addEventListener('click', (headerBtn) => {
    const headerNav = document.querySelector('.header-nav');
    const headerBtnIcon = document.querySelector('.header-button__icon');
    headerNav.classList.toggle('header-nav--active');
    headerBtnIcon.classList.toggle('header-button__icon--active');
})




