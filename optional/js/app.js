'use strict'
// ### Данное задание не обязательно для выполнения

// ## Задание

// Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Написать функцию для подсчета n-го обобщенного [числа Фибоначчи](https://ru.wikipedia.org/wiki/%D0%A7%D0%B8%D1%81%D0%BB%D0%B0_%D0%A4%D0%B8%D0%B1%D0%BE%D0%BD%D0%B0%D1%87%D1%87%D0%B8). Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
// - Считать с помощью модального окна браузера число, которое введет пользователь (n).
// - С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
// - Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

// #### Литература:
// - [Рекурсия, стек](https://learn.javascript.ru/recursion)
// function fibo(n) {
//     if (n  <= 1) {
//         return n;
//     }
//     return fibo(n - 1) + fibo(n - 2);
// }
// console.log(fibo(prompt("")));

// ### Дане завдання не обов'язкове для виконання

// ## Завдання

// Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Створити порожній об'єкт student, з полями name та lastName.
// - Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// - У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel. 
// - порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// - Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.

// #### Література:
// - [Об'єкти як асоціативні масиви](https://learn.javascript.ru/object)
// - [Перебір властивостей об'єктів](https://learn.javascript.ru/object-for-in)


// function uchenik(name, lastName, lessonName, rating) {
//     name = prompt('input your name');
//     lastName = prompt('input your last name');
//     let student = {
//         name: name,
//         lastName: lastName,
//         lessonName: lessonName,
//         rating:rating,
//         getTabel: function () {
//             for (let i = 0; i <= 4; i++) {
//                 lessonName = prompt('name of lesson');
//                 rating = prompt('received rating');
//                 this.getTabel[i++];
//             }
//             return this.name + " " + this.lastName + " / " + lessonName + " - " + rating;
//         }
//     }
//     return student;
// }
// let user = uchenik();
// console.log(user.getTabel());
// window.onbeforeunload = function() {
//   return "Есть несохранённые изменения. Всё равно уходим?";
// };
const student = {
  name: prompt("Imya"),
  lastName: prompt("Familiya"),
  tabel: {
  },
  newKey(predmet, otsinka) {
    if (predmet in student.tabel) {
      console.log("Bylo");
    } else {
      student.tabel[predmet] = otsinka;
    }
  },
};
let predmet = "";
let otsinka = 0;
do {
  predmet = prompt("predmet");
  otsinka = prompt("otsinka");
  student.newKey(predmet, otsinka);
} while (predmet !== null || otsinka !== null);
delete student.tabel.null;
console.log(student.name);
console.log(student.lastName);
// console.log(student);
for (const key in student.tabel) {
    console.log(key + " = " + student.tabel[key]);
}
