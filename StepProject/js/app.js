'use sctrict'

// ----disable-links --
let links = document.querySelectorAll('a');
links.forEach(el => el.addEventListener("click", disableLinks, false));
function disableLinks(e) {
    e.returnValue = false;
}
// --section-services-------
$('.services-description-item:first-child').addClass('services-description-item_active');
$('.services-description-content-text').hide();
$('.services-description-content-text:first').show();

$('.services-description-triangle').attr("style", 'display:block');
$('.services-description-triangle').hide();
$('.services-description-triangle:first').show();

$('.services-description-item').click(function (e) {
    $('.services-description-item').removeClass('services-description-item_active');
    $('.services-description-triangle').attr("style", 'display:none');
    $(this).addClass('services-description-item_active');
    $('.services-description-item_active').children('.services-description-triangle').attr("style", 'display:block');
    $('.services-description-content-text').hide();
    $('.services-description-triangle').hide();
    let activeTab = $(this).find('a').attr('href');
    let activeTriangle = $('.services-description-item_active').children('.services-description-triangle').attr("style", 'display');
    $.each([activeTab, activeTriangle], function(i, el) {
        $(el).fadeIn();
});  
    return false;
});

// ------------------------ section-work -------------------
const workLoad = document.querySelectorAll('.work-grid-load');
const workButton = document.getElementById('workBtn');
const workPost = document.querySelectorAll('.work-grid-post');
let workItems = document.querySelectorAll(".work-item");
let activeWorkItem = document.querySelector(".work-item.work-item_border");
// active work-item----
workItems.forEach(el => {
    el.addEventListener("click", function () {
        el.classList.add("work-item_border");
        if (activeWorkItem != null) {
            activeWorkItem.classList.remove("work-item_border");
        }
        activeWorkItem = el;
    });
})
//  work-filter----
workLoad.forEach(elem => {
    elem.classList.add('work-grid-load_hide');
})
workButton.addEventListener('click', elem => {
    workLoad.forEach(elem => {
        elem.classList.remove('work-grid-load_hide');;
    })
    workButton.remove();
})

function filter() {
    document.querySelector('.work-list').addEventListener('click', event => {
        if (!event.target.classList.contains('work-item')) return false;
        let filterClass = event.target.dataset['filter'];
        workPost.forEach(elem => {
            elem.classList.remove('work-grid-post_hide');
            if (!elem.classList.contains(filterClass) && filterClass !== 'all') {
                elem.classList.add('work-grid-post_hide')
            }
        })
    })
}
filter()


// -------------------------about-------------
$(document).ready(function () {
    $(".slider").slick({
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        speed: 1500,
        easing: 'ease',
        infinite: true,
        initialSlide: 1,
        draggable: true,
        asNavFor: ".slider-big",
        variableWidth: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '20px'
    });
})
$(document).ready(function () {
    $(".slider-big").slick({
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        speed: 1500,
        easing: 'ease',
        infinite: true,
        initialSlide: 4,
        draggable: true,
        asNavFor: ".slider",
    });
})
